function background(){
    document.body.classList.add('animateBackground')
}
function toTop(){
    let fullBody = document.querySelector('.hiddenMain');
    fullBody.classList.add('newAnim');
}
function headerMoving(){
    let head = document.querySelector('.headerFull');
    head.classList.add('headImg');
}
function overFlow(){
    let overF = document.querySelector('.hiddenFlow');
    overF.classList.remove('hiddenFlow');
}
function imgOpacAdd(){
    let head = document.querySelector('.headerFull');
    head.classList.add('headOpacity');
}
function imgOpacRem(){
    let head = document.querySelector('.headerFull');
    head.classList.remove('headOpacity');
}
imgOpacAdd();
setTimeout(imgOpacRem, 2000);
setTimeout(background,2100);
setTimeout(headerMoving, 2000);
setTimeout(toTop,2000);
setTimeout(overFlow,2000);
